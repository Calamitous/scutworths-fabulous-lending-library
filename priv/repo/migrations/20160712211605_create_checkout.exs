defmodule ScutworthsFabulousLendingLibrary.Repo.Migrations.CreateCheckout do
  use Ecto.Migration

  def change do
    create table(:checkouts) do
      add :book_id, :integer
      add :borrower_id, :integer
      add :borrowed_on, :date
      add :returned_on, :date

      timestamps()
    end

  end
end
