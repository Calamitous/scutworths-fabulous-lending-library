defmodule ScutworthsFabulousLendingLibrary.Repo.Migrations.CreateBorrower do
  use Ecto.Migration

  def change do
    create table(:borrowers) do
      add :name, :string
      add :email, :string

      timestamps()
    end

  end
end
