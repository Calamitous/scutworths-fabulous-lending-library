# Scutworth's Fabulous Lending Library
### "Who's got my stuff!?" --Old Man Scutworth

To install:

  * `docker-compose up`
  * `docker-compose run --rm web ecto.create`
  * `docker-compose run --rm web ecto.migrate`

Server will be running on localhost:4001

