defmodule ScutworthsFabulousLendingLibrary.PageController do
  use ScutworthsFabulousLendingLibrary.Web, :controller

  def index(conn, _params) do
    redirect(conn, to: checkout_path(conn, :index))
  end
end
