defmodule ScutworthsFabulousLendingLibrary.CheckoutController do
  use ScutworthsFabulousLendingLibrary.Web, :controller

  alias ScutworthsFabulousLendingLibrary.Checkout
  alias ScutworthsFabulousLendingLibrary.Book
  alias ScutworthsFabulousLendingLibrary.Borrower

  def index(conn, _params) do
    checkouts = Repo.all from c in Checkout, preload: [:book, :borrower]
    render(conn, "index.html", checkouts: checkouts)
  end

  def new(conn, _params) do
    changeset = Checkout.changeset(%Checkout{})

    books = Repo.all from b in Book, select: { b.title, b.id }
    borrowers = Repo.all from b in Borrower, select: { b.name, b.id }

    render(conn, :new, %{changeset: changeset, books: books, borrowers: borrowers})
  end

  def create(conn, %{"checkout" => checkout_params}) do
    changeset = Checkout.changeset(%Checkout{}, checkout_params)

    case Repo.insert(changeset) do
      {:ok, _checkout} ->
        conn
        |> put_flash(:info, "Checkout created successfully.")
        |> redirect(to: checkout_path(conn, :index))
      {:error, changeset} ->
        books = Repo.all from b in Book, select: { b.title, b.id }
        borrowers = Repo.all from b in Borrower, select: { b.name, b.id }

        render(conn, :new, %{ changeset: changeset, books: books, borrowers: borrowers})
    end
  end

  def show(conn, %{"id" => id}) do
    checkout = Repo.get!(Checkout, id)

    books = Repo.all from b in Book, select: { b.title, b.id }
    borrowers = Repo.all from b in Borrower, select: { b.name, b.id }

    render(conn, "show.html", %{checkout: checkout, books: books, borrowers: borrowers})
  end

  def edit(conn, %{"id" => id}) do
    checkout = Repo.get!(Checkout, id)
    changeset = Checkout.changeset(checkout)

    books = Repo.all from b in Book, select: { b.title, b.id }
    borrowers = Repo.all from b in Borrower, select: { b.name, b.id }

    render(conn, "edit.html", %{ checkout: checkout, changeset: changeset, books: books, borrowers: borrowers})
  end

  def update(conn, %{"id" => id, "checkout" => checkout_params}) do
    checkout = Repo.get!(Checkout, id)
    changeset = Checkout.changeset(checkout, checkout_params)

    books = Repo.all from b in Book, select: { b.title, b.id }
    borrowers = Repo.all from b in Borrower, select: { b.name, b.id }

    case Repo.update(changeset) do
      {:ok, checkout} ->
        conn
        |> put_flash(:info, "Checkout updated successfully.")
        |> redirect(to: checkout_path(conn, :show, checkout))
      {:error, changeset} ->
        render(conn, :edit, %{ checkout: checkout, changeset: changeset, books: books, borrowers: borrowers})
    end
  end

  def delete(conn, %{"id" => id}) do
    checkout = Repo.get!(Checkout, id)
    Repo.delete!(checkout)

    conn
    |> put_flash(:info, "Checkout deleted successfully.")
    |> redirect(to: checkout_path(conn, :index))
  end
end
