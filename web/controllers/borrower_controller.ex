defmodule ScutworthsFabulousLendingLibrary.BorrowerController do
  use ScutworthsFabulousLendingLibrary.Web, :controller

  alias ScutworthsFabulousLendingLibrary.Borrower

  def index(conn, _params) do
    borrowers = Repo.all(Borrower)
    render(conn, "index.html", borrowers: borrowers)
  end

  def new(conn, _params) do
    changeset = Borrower.changeset(%Borrower{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"borrower" => borrower_params}) do
    changeset = Borrower.changeset(%Borrower{}, borrower_params)

    case Repo.insert(changeset) do
      {:ok, borrower} ->
        conn
        |> put_flash(:info, "Borrower \"#{borrower.name}\" created.")
        |> redirect(to: borrower_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    borrower = Repo.get!(Borrower, id)
    render(conn, "show.html", borrower: borrower)
  end

  def edit(conn, %{"id" => id}) do
    borrower = Repo.get!(Borrower, id)
    changeset = Borrower.changeset(borrower)
    render(conn, "edit.html", borrower: borrower, changeset: changeset)
  end

  def update(conn, %{"id" => id, "borrower" => borrower_params}) do
    borrower = Repo.get!(Borrower, id)
    changeset = Borrower.changeset(borrower, borrower_params)

    case Repo.update(changeset) do
      {:ok, borrower} ->
        conn
        |> put_flash(:info, "Borrower updated successfully.")
        |> redirect(to: borrower_path(conn, :show, borrower))
      {:error, changeset} ->
        render(conn, "edit.html", borrower: borrower, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    borrower = Repo.get!(Borrower, id)
    Repo.delete!(borrower)

    conn
    |> put_flash(:info, "Borrower deleted successfully.")
    |> redirect(to: borrower_path(conn, :index))
  end
end
