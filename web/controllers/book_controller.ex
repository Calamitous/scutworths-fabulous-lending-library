defmodule ScutworthsFabulousLendingLibrary.BookController do
  use ScutworthsFabulousLendingLibrary.Web, :controller
  alias ScutworthsFabulousLendingLibrary.Book

  def index(conn, _params) do
    books = Repo.all(Book)
    render conn, :index, books: books
  end

  def new(conn, _params) do
    render conn, :new, changeset: Book.changeset(%Book{})
  end

  def create(conn, %{"book" => book}) do
    changeset = Book.changeset(%Book{}, book)
    case Repo.insert(changeset) do
      {:ok, book} ->
        conn
        |> put_flash(:info, "Book \"#{book.title}\" saved.")
        |> redirect(to: "/books")
      {:error, changeset} ->
        render(conn, :new, changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    book = Repo.get(Book, id)
    changeset = Book.changeset(book)

    render conn, :edit, %{book: book, changeset: changeset}
  end

  def update(conn, %{"id" => id, "book" => book_params}) do
    book = Repo.get(Book, id)
    changeset = Book.changeset(book, book_params)
    case Repo.update(changeset) do
      {:ok, book} ->
        conn
        |> put_flash(:info, "Book \"#{book.title}\" saved.")
        |> redirect(to: "/books")
      {:error, changeset} ->
        render(conn, :edit, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    book = Repo.get!(Book, id)
    Repo.delete!(book)

    conn
    |> put_flash(:info, "Book deleted.")
    |> redirect(to: book_path(conn, :index))
  end

  def show(conn, %{"id" => id}) do
    book = Repo.get(Book, id)
    render conn, :show, book: book
  end
end
