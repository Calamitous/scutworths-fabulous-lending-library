defmodule ScutworthsFabulousLendingLibrary.Book do
  use ScutworthsFabulousLendingLibrary.Web, :model

  schema "books" do
    field :title, :string
    field :authors, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :authors])
    |> validate_required([:title, :authors])
  end
end
