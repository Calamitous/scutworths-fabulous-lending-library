defmodule ScutworthsFabulousLendingLibrary.Borrower do
  use ScutworthsFabulousLendingLibrary.Web, :model

  schema "borrowers" do
    field :name, :string
    field :email, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :email])
    |> validate_required([:name, :email])
  end
end
