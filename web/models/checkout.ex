defmodule ScutworthsFabulousLendingLibrary.Checkout do
  use ScutworthsFabulousLendingLibrary.Web, :model

  schema "checkouts" do
    field :borrowed_on, Ecto.Date
    field :returned_on, Ecto.Date

    belongs_to :book, ScutworthsFabulousLendingLibrary.Book
    belongs_to :borrower, ScutworthsFabulousLendingLibrary.Borrower

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:book_id, :borrower_id, :borrowed_on, :returned_on])
    |> validate_required([:book_id, :borrower_id, :borrowed_on])
  end
end
