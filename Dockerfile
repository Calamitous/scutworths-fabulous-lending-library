FROM elixir:1.3

ENV PHOENIX_VERSION "1.2.0"

RUN \
  curl -sL https://deb.nodesource.com/setup_6.x | bash - && \
  apt-get install -y nodejs && \
  apt-get clean && \
  cd /var/lib/apt/lists && rm -fr *Release* *Sources* *Packages* && \
  truncate -s 0 /var/log/*log && \
  npm install -g brunch --quiet

RUN \
  mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phoenix_new-$PHOENIX_VERSION.ez

RUN \
  apt-get update && \
  apt-get install -y inotify-tools postgresql-client && \
  apt-get clean && \
  cd /var/lib/apt/lists && rm -fr *Release* *Sources* *Packages* && \
  truncate -s 0 /var/log/*log

RUN \
  mix local.hex --force && \
  mix hex.info && \
  mix local.rebar --force

WORKDIR /code
COPY . /code

CMD ["mix", "phoenix.server"]
