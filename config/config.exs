# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :app,
  namespace: ScutworthsFabulousLendingLibrary,
  ecto_repos: [ScutworthsFabulousLendingLibrary.Repo]

# Configures the endpoint
config :app, ScutworthsFabulousLendingLibrary.Endpoint,
  url: [host: "0.0.0.0"],
  secret_key_base: "7hyJS4jk769g5pqWPUzcV+ESoMaOacCSF8rp1WK8z3E8RFZm1ru6nmS9M/OFF9i5",
  render_errors: [view: ScutworthsFabulousLendingLibrary.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ScutworthsFabulousLendingLibrary.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
