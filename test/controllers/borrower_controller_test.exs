defmodule ScutworthsFabulousLendingLibrary.BorrowerControllerTest do
  use ScutworthsFabulousLendingLibrary.ConnCase

  alias ScutworthsFabulousLendingLibrary.Borrower
  @valid_attrs %{email: "some content", name: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, borrower_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing borrowers"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, borrower_path(conn, :new)
    assert html_response(conn, 200) =~ "New borrower"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, borrower_path(conn, :create), borrower: @valid_attrs
    assert redirected_to(conn) == borrower_path(conn, :index)
    assert Repo.get_by(Borrower, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, borrower_path(conn, :create), borrower: @invalid_attrs
    assert html_response(conn, 200) =~ "New borrower"
  end

  test "shows chosen resource", %{conn: conn} do
    borrower = Repo.insert! %Borrower{}
    conn = get conn, borrower_path(conn, :show, borrower)
    assert html_response(conn, 200) =~ "Show borrower"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, borrower_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    borrower = Repo.insert! %Borrower{}
    conn = get conn, borrower_path(conn, :edit, borrower)
    assert html_response(conn, 200) =~ "Edit borrower"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    borrower = Repo.insert! %Borrower{}
    conn = put conn, borrower_path(conn, :update, borrower), borrower: @valid_attrs
    assert redirected_to(conn) == borrower_path(conn, :show, borrower)
    assert Repo.get_by(Borrower, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    borrower = Repo.insert! %Borrower{}
    conn = put conn, borrower_path(conn, :update, borrower), borrower: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit borrower"
  end

  test "deletes chosen resource", %{conn: conn} do
    borrower = Repo.insert! %Borrower{}
    conn = delete conn, borrower_path(conn, :delete, borrower)
    assert redirected_to(conn) == borrower_path(conn, :index)
    refute Repo.get(Borrower, borrower.id)
  end
end
