defmodule ScutworthsFabulousLendingLibrary.CheckoutControllerTest do
  use ScutworthsFabulousLendingLibrary.ConnCase

  alias ScutworthsFabulousLendingLibrary.Checkout
  @valid_attrs %{book_id: 42, borrowed_on: %{day: 17, month: 4, year: 2010}, borrower_id: 42, returned_on: %{day: 17, month: 4, year: 2010}}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, checkout_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing checkouts"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, checkout_path(conn, :new)
    assert html_response(conn, 200) =~ "New checkout"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, checkout_path(conn, :create), checkout: @valid_attrs
    assert redirected_to(conn) == checkout_path(conn, :index)
    assert Repo.get_by(Checkout, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, checkout_path(conn, :create), checkout: @invalid_attrs
    assert html_response(conn, 200) =~ "New checkout"
  end

  test "shows chosen resource", %{conn: conn} do
    checkout = Repo.insert! %Checkout{}
    conn = get conn, checkout_path(conn, :show, checkout)
    assert html_response(conn, 200) =~ "Show checkout"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, checkout_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    checkout = Repo.insert! %Checkout{}
    conn = get conn, checkout_path(conn, :edit, checkout)
    assert html_response(conn, 200) =~ "Edit checkout"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    checkout = Repo.insert! %Checkout{}
    conn = put conn, checkout_path(conn, :update, checkout), checkout: @valid_attrs
    assert redirected_to(conn) == checkout_path(conn, :show, checkout)
    assert Repo.get_by(Checkout, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    checkout = Repo.insert! %Checkout{}
    conn = put conn, checkout_path(conn, :update, checkout), checkout: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit checkout"
  end

  test "deletes chosen resource", %{conn: conn} do
    checkout = Repo.insert! %Checkout{}
    conn = delete conn, checkout_path(conn, :delete, checkout)
    assert redirected_to(conn) == checkout_path(conn, :index)
    refute Repo.get(Checkout, checkout.id)
  end
end
