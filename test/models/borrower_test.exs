defmodule ScutworthsFabulousLendingLibrary.BorrowerTest do
  use ScutworthsFabulousLendingLibrary.ModelCase

  alias ScutworthsFabulousLendingLibrary.Borrower

  @valid_attrs %{email: "some content", name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Borrower.changeset(%Borrower{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Borrower.changeset(%Borrower{}, @invalid_attrs)
    refute changeset.valid?
  end
end
