defmodule ScutworthsFabulousLendingLibrary.CheckoutTest do
  use ScutworthsFabulousLendingLibrary.ModelCase

  alias ScutworthsFabulousLendingLibrary.Checkout

  @valid_attrs %{book_id: 42, borrowed_on: %{day: 17, month: 4, year: 2010}, borrower_id: 42, returned_on: %{day: 17, month: 4, year: 2010}}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Checkout.changeset(%Checkout{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Checkout.changeset(%Checkout{}, @invalid_attrs)
    refute changeset.valid?
  end
end
